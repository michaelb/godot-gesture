onready var gestures = get_node('GestureDetector')
onready var touches = get_node('TouchLabel')
onready var block = get_node('Block')
onready var elabel = get_node('EventLabel')
onready var clabel = get_node('CLabel')
onready var anim = get_node('AnimationPlayer')

func _on_ready():
    block.set_modulate(Color(0, 0, 0))

func _on_GestureDetector_touches_changed(touch_count):
    touches.set_text('Touches: ' + str(touch_count))

func _on_GestureDetector_double_tap(pos):
    block.set_pos(pos)
    block.set_modulate(Color(1, 0, 0))
    elabel.set_text('double tapped')
    anim.play('flashevent')

func _on_GestureDetector_tap(pos):
    #anim.play('fadeout')
    #elabel.set_opacity(0)
    block.set_pos(pos)
    block.set_modulate(Color(0, 0, 1))

func _on_GestureDetector_twisted(pos, is_twist_cw):
    var desc = 'clockwise' if is_twist_cw else 'counter-clockwise'
    elabel.set_text('twisted ' + desc)
    block.set_pos(pos)
    block.set_modulate(Color(0, 1, 1))
    print("TWISTED")
    anim.play('flashevent')

func _on_GestureDetector_pinched(pos, is_pinch_in):
    var desc = 'in' if is_pinch_in else 'out'
    elabel.set_text('pinched ' + desc)
    block.set_modulate(Color(1, 1, 0))
    block.set_pos(pos)
    print("PINCHED")
    anim.play('flashevent')

func _on_GestureDetector_dragged(pos, drag_vec):
    elabel.set_text('dragged ' + str(drag_vec))
    block.set_pos(pos)
    block.set_modulate(Color(1, 0, 1))
    print("DRAGGED")
    anim.play('flashevent')

func _on_GestureDetector_multi_twist():
    clabel.set_text('twisting')

func _on_GestureDetector_multi_pinch():
    clabel.set_text('pinching')

func _on_GestureDetector_multi_drag():
    clabel.set_text('dragging')

func _on_GestureDetector_single_drag(ev):
    print("Single drag")
    elabel.set_text('single drag ' + str(ev.pos))
    block.set_modulate(Color(0.5, 1, 0.5))
    anim.play('flashevent')

func _on_GestureDetector_single_tap(ev):
    print("Single tap")
    elabel.set_text('single tap ' + str(ev.pos))
    block.set_modulate(Color(1, 0.5, 0.5))
    anim.play('flashevent')
