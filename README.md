# godot-gesture

Helper node for making [Godot](http://godotengine.org/) engine games
react to different gestures on mobile.  Godot Engine is an awesome free
software game engine that you should check out!

## Reliability

As I found out, detecting gestures is really hard, and there are
probably better algorithms out there than the ones I hacked together
for this. While it seems to be acceptably accurate for the projects I'm
working on, it could probably use a lot of improvement and extra
features, so suggestions and/or pull-requests are welcome!

## Usage

* Instance the scene `GodotGesture.tscn` somewhere in your scene
  hierarchy

* Attach events for the ones being listened to

## Signals

Godot Gesture is entirely signal based.

### Tap events

* `double_tap(position)` - Tap twice somewhere on the screen

* `tap(position)` - A single tap on the screen, called the first time
  the screen is tapped.

* `touches_changed(touch_count)` - Called whenever the number of
  fingers currently touching the screen changes.

### Multi-touch events

* `twisted(position, is_clockwise)` - Clockwise or counter-clockwise
  two-finger "twist" motion

* `pinched(position, is_inward)` - Inward or outward pinch (e.g. zoom)
  gesture.

* `dragged(position, drag_vector)` - Two finger drag event.


### Debounced events

These are called when one of the `listen_for_debounced_*` properties is
enabled. It is called `debounce_time_ms` from after when the event
actually occurred. They are *not called* if the event is actually part of
one of the above events (e.g., if the drag or tap later turns out to be
a multi-touch or double tap event). This is useful for distinguishing
between multitouch events and taps or drags by themselves.

* `single_tap(event)` - A single finger, stray tap

* `single_drag(event)` - A single finger, stray drag

## Settings

Fine control over threshholds for detecting the various events is
available via exported script variables. Checking for each event type
can be turned on or off with the following events:

* `listen_for_multi_drag = true`
* `listen_for_multi_pinch = true`
* `listen_for_multi_twist = true`

This is useful if you only care about certain events, and other events
are getting detected instead (e.g. you only care about pinch, but drag
or twist events are getting registered instead).

* `double_tap_thresh_ms = 500` - Maximum separation in miliseconds for
  click event
* `double_tap_thresh_distance = 100` - Maximum separation between taps
  for a double tap event to be considered
* `twist_precision = 150` - Higher means more twist events
* `pinch_precision = 70` - Lower means more pinch events
* `drag_precision = 215` - Higher means more drag events
* `drag_threshhold = 10` - Filters brief "drag" events
* `deobunce_time_ms = 600` - How long debounced events are cached
  before being triggered. If you are listening to `double_tap`, make
  sure this is a little longer than `double_tap_thresh_ms`.


### Debug settings

These probably are not very useful unless you are debugging, or
intending to contribute to `godot-gesture` itself.

* `debug_right_click_enabled = false` - For debugging / development only: Combine this with Godot's
  simulate touch events project setting (set
  `display/emulate_touchscreen` equal to `true`) to test gestures on
  the computer. Right click to "plant" an "extra finger" in a location,
  and right click again to lift it up. All events except for 2 finger
  drag can be tested this way (since dragging requires 2 fingers both
  moving).

* `debug_show_clicks_and_drag = false` - For debugging / development
  only: Creates symbols on screen to show where events are getting
  noticed

[Example debugging in use](http://i.imgur.com/RbrR8v2.gif)
